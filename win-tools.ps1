param (
    [string]$Dir = "$env:userprofile\opt"
)

#Requires -RunAsAdministrator

Function Update-Windows {
    Write-Host "[+] Installing NuGet"
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
    Write-Host "[+] Doing a Windows Update"  
    Install-Module PSWindowsUpdate -Force
    Get-WindowsUpdate -AcceptAll
    Install-WindowsUpdate -AcceptAll
}

Function Install-Tools {
    Write-Host "[+] Downloading and Installing Visual Studio Community"    
    $outfile = ".\vs_Community.exe"
    Invoke-WebRequest -Uri "https://download.visualstudio.microsoft.com/download/pr/17a0244e-301e-4801-a919-f630bc21177d/f07e4f9838e855c0368576952b7430b5d446784c6c7d1d6e0f35fbfe00199e84/vs_Community.exe" -OutFile $outfile
    Start-Process -FilePath $outfile -ArgumentList "-p" -Wait
    Remove-Item $outfile
    Write-Host "[+] Downloading and Installing git"    
    $outfile = ".\git.exe"
    Invoke-WebRequest -Uri "https://github.com/git-for-windows/git/releases/download/v2.26.2.windows.1/Git-2.26.2-64-bit.exe" -OutFile $outfile
    Start-Process -filePath $outfile -ArgumentList "/SILENT" -Wait
    Remove-Item $outfile
    Write-Host "[+] Downloading and Installing Cutter"
    $outfile = ".\cutter.zip"
    Invoke-WebRequest -Uri "https://github.com/radareorg/cutter/releases/download/v1.10.3/Cutter-v1.10.3-x64.Windows.zip" -OutFile $outfile
    Expand-Archive -Path $outfile
    Remove-Item $outfile
    Write-Host "[+] Downloading and Installing x64dbg"
    $outfile = ".\x64dbg.zip"
    Invoke-WebRequest -Uri "https://github.com/x64dbg/x64dbg/releases/download/snapshot/snapshot_2020-09-14_20-29.zip" -Outfile $outfile
    Expand-Archive -Path $outfile
    Remove-Item $outfile
    Write-Host "[+] Downloading and Installing 7zip"
    $outfile = ".\7zip.msi"
    Invoke-WebRequest -Uri "https://www.7-zip.org/a/7z1900-x64.msi" -OutFile $outfile
    Start-Process -FilePath $outfile -ArgumentList "/quiet" -Wait
    Remove-Item $outfile
    Write-Host "[+] Downloading and Installing FireFox"
    $outfile = ".\Firefox_Installer.exe"
    Invoke-WebRequest -Uri "https://download-installer.cdn.mozilla.net/pub/firefox/releases/77.0.1/win32/en-US/Firefox%20Installer.exe" -OutFile $outfile
    Start-Process -FilePath $outfile -Wait
    Remove-Item $outfile
    Write-Host "[+] Downloading dnSpy"
    $outfile = ".\dnSpy.zip"
    Invoke-WebRequest -Uri "https://github.com/0xd4d/dnSpy/releases/download/v6.1.7/dnSpy-net472.zip" -Outfile $outfile
    Expand-Archive -Path $outfile
    Remove-Item $outfile
    Write-Host "[+] Downloading Sysinternals Suite"
    $outfile = ".\SysinternalsSuite.zip"
    Invoke-WebRequest -Uri "https://download.sysinternals.com/files/SysinternalsSuite.zip" -Outfile $outfile
    Expand-Archive -Path $outfile
    Remove-Item $outfile
    Write-Host "[+] Installing Wireshark"
    $outfile = "wireshark.exe"
    Invoke-WebRequest -Uri "https://1.eu.dl.wireshark.org/win64/Wireshark-win64-3.2.6.exe" -Outfile $outfile
    Start-Process -FilePath $outfile -ArgumentList "/S" -Wait
    Remove-Item $outfile
    Write-Host "[+] Downloading Explorer Suite"
    $outfile = "ExplorerSuite.exe"
    Invoke-WebRequest -Uri "https://ntcore.com/files/ExplorerSuite.exe" -Outfile $outfile
    Write-Host "[+] Installing Echo Mirage"
    $outfile = "EchoMirage-3.1.exe"
    Invoke-WebRequest -Uri "https://kumisystems.dl.sourceforge.net/project/echomirage.oldbutgold.p/EchoMirage-3.1.exe" -Outfile $outfile
    Start-Process -FilePath $outfile -ArgumentList "/SILENT" -Wait
    Remove-Item $outfile
    Write-Host "[+] Installing Process Hacker"
    $outfile = "processhacker-2.39-setup.exe"
    Invoke-WebRequest -Uri "https://github.com/processhacker/processhacker/releases/download/v2.39/processhacker-2.39-setup.exe" -Outfile $outfile
    Start-Process -FilePath $outfile -ArgumentList "/SILENT" -Wait
    Remove-Item $outfile    
    #Write-Host "[+] Downloading Notepad++"
    #$outfile = "npp.7.8.8.Installer.x64.exe"
    #Invoke-WebRequest -Uri "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v7.8.8/npp.7.8.8.Installer.x64.exe" -Outfile $outfile
    Write-Host "[+] Downloading LINQPad"
    $outfile = "LINQPad6Setup.exe"
    Invoke-WebRequest -Uri "https://www.linqpad.net/GetFile.aspx?LINQPad6Setup.exe" -Outfile $outfile  
    Start-Process -FilePath $outfile -ArgumentList "/SILENT" -Wait
}

Function Install-PSModules {
    Write-Host "[+] Installing PowerCLI Module"
    Install-Module -Name VMware.PowerCLI -AllowClobber -Scope CurrentUser -Force
    Write-Host "[+] Installing Az Module"
    Install-Module -Name Az -AllowClobber -Scope CurrentUser -Force
}

Function Get-Gits {
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/pwntester/ysoserial.net" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/pwntester/ViewStatePayloadGenerator" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/0xacb/viewgen" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/Illuminopi/RCEvil.NET" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/BloodHoundAD/SharpHound3" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/S3cur3Th1sSh1t/PowerSharpPack" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/cobbr/SharpSploit" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/GhostPack/Rubeus" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/NetSPI/PESecurity" -Wait
    Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/mattifestation/PowerShellArsenal" -Wait
      #Start-Process "C:\Program Files\Git\cmd\git.exe" -ArgumentList "clone", "https://github.com/PowerShellMafia/PowerSploit/" -Wait
}

Function Tweak-Windows {
    Write-Host "[+] Trying to disable Defender" # issues
    Set-MpPreference -DisableRealtimeMonitoring $true
    Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Value 1 -Force
    Set-MpPreference -DisableRealtimeMonitoring $true
    Write-Host "[+] Disabling that pesky firewall"
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False
    Write-Host "[+] Changing Power Scheme to High Peformance"
    PowerCfg -SetActive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
    Write-Host "[+] Installing SSH Server"
    Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
    Set-Service -Name sshd -StartupType 'Automatic'
    New-ItemProperty -Path "HKLM:\SOFTWARE\OpenSSH" -Name DefaultShell -Value "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -PropertyType String -Force
    Write-Host "[+] Enabling RDP"
    Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
    Write-Host "[+] Setting Dark Theme"
    Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize" -Name "AppsUseLightTheme" -Value 0
}

##Main##
Update-Windows
Tweak-Windows
Install-PSModules
New-Item -ItemType Directory -Force -Path $Dir
Set-Location -Path $Dir
Install-Tools
Get-Gits
