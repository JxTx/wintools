# winTools

not every tool is on Linux 

## Why
Well I dont like pushing buttons, and automation is the way forward. Too many times i've needed $x tool thats only avaliable on Windows (its its way easier to use) so I made this script. Its far from perfect, and I'm awear of other more full projects, but where is the learning in that ;)

## Usage
You have to set the execution policy to allow you to run this script. Because I dont really know what they mean, i've been setting it to `Unrestrected` during testing, but I'm 90% sure that may be overkill.

```
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
```

The script requires a PowerShell instance to be ran as administrator too. then just `.\name-of-script.ps1`

## Tools
Its very vanilla right now, but heres a list of tools.

- Visual Studio Community
- git
- Python3.8.3
- Cutter
- x64dbg
- 7Zip
- Firefox
- VMWare PowerCLI
- Az

## Settings
The intention is to run this in a VM and RDP/SSH to it when required. A bunch of settings to enable this have been configured and I invite you to read the code.

## Contributions
I dont know what i'm doing, I dont know if i've missed any best ways to do it.. all i know is that it works for me. I'd love for anyone to want to open a merge request!!